<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    protected $table='mahasiswa';
    protected $guarded=[];

    public function user(){
        return $this->morphOne(User::class,'role');
    }
}
