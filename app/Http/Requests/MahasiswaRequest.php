<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nim'=>['required','integer:10'],
            'nama'=>['required','string'],
            'fakultas'=>['required','string'],
            'jurusan'=>['required','string'],
            'no_hp'=>['required','numeric'],
            'no_wa'=>['required','numeric'],
        ];
    }
}
