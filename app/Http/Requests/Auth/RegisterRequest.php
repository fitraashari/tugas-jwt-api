<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['alpha_num','min:6','max:25','required','unique:users,username'],
            'email' => ['email','required','unique:users,email'],
            'password'=>['required','min:6',''],
            'role_type'=>['required','max:10','in:admin,mahasiswa'],
            'nim'=>['required_if:role_type,mahasiswa','integer','digits_between:1,10'],
            'nama'=>['required_if:role_type,mahasiswa','string'],
            'fakultas'=>['required_if:role_type,mahasiswa','string'],
            'jurusan'=>['required_if:role_type,mahasiswa','string'],
            'no_hp'=>['required_if:role_type,mahasiswa','numeric'],
            'no_wa'=>['required_if:role_type,mahasiswa','numeric'],
        ];
    }
}
