<?php

namespace App\Http\Controllers;

use App\Http\Requests\MahasiswaRequest;
use App\Http\Resources\MahasiswaCollection;
use App\Http\Resources\MahasiswaResource;
use App\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mahasiswa = Mahasiswa::get();
        return new MahasiswaCollection($mahasiswa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MahasiswaRequest $request)
    {
        //
        
        $mahasiswa = Mahasiswa::create($this->mahasiswaStore());
        return new MahasiswaResource($mahasiswa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {
        //
        return new MahasiswaResource($mahasiswa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MahasiswaRequest $request, Mahasiswa $mahasiswa)
    {
        //
        $mahasiswa->update($this->mahasiswaStore());
        return new MahasiswaResource($mahasiswa);
    }
    public function updateMe(MahasiswaRequest $request){
        $mahasiswa = auth()->user()->role()->update($this->mahasiswaStore());
        return new MahasiswaResource(auth()->user()->role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mahasiswa $mahasiswa)
    {
        //
        $mahasiswa->delete();
        return response()->json('Delete Berhasil',200);
    }
    public function mahasiswaStore(){
        return [
            'nim' => request('nim'),
            'nama' => request('nama'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
        ];
    }
}
