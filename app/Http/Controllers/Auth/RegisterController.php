<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Mahasiswa;
use App\User;
// use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //
        if(request('role_type')=="mahasiswa"){
            //dd($request->all());
            $mahasiswa = Mahasiswa::create([
                'nim'=>request('nim'),
                'nama'=>request('nama'),
                'fakultas'=>request('fakultas'),
                'jurusan'=>request('jurusan'),
                'no_hp'=>request('no_hp'),
                'no_wa'=>request('no_wa')
            ]);
            User::create([
                'username'=>request('username'),
                'email'=>request('email'),
                'password'=>bcrypt(request('password')),
                'role_id'=>$mahasiswa->id,
                'role_type'=>request('role_type')
            ]);
        }else{
            User::create([
                'username'=>request('username'),
                'email'=>request('email'),
                'password'=>bcrypt(request('password')),
                'role_type'=>request('role_type')
            ]);
        }
        //dd($request->all());
        
        return response('Terimakasih, pendaftaran anda berhasil!',200);
    }
}
