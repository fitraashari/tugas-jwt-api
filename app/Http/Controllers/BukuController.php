<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Http\Requests\BukuRequest;
use App\Http\Resources\BukuCollection;
use App\Http\Resources\BukuResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $buku = Buku::get();
        return new BukuCollection($buku);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BukuRequest $request)
    {
        //
        //dd(Auth::user()->username);
        $buku = Buku::create($this->bookStore());
        return $buku;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $buku)
    {
        //
        return new BukuResource($buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Buku $buku)
    {
        //
        //dd($buku);
        request()->validate([
            'kode_buku'=>'required','unique:buku,kode_buku',''.$request->kode_buku.'',
            'judul'=>['required'],
            'pengarang'=>['required'],
            'tahun_terbit'=>['required','max:4']
        ]);
        $buku->update($this->bookStore());
        return new BukuResource($buku);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $buku)
    {
        //
        $buku->delete();

        return response()->json('Delete Buku Berhasil',200);
    }
    public function bookStore(){
        return ['kode_buku'=>request('kode_buku'),
        'judul'=>request('judul'),
        'pengarang'=>request('pengarang'),
        'tahun_terbit'=>request('tahun_terbit'),
    ];
    }
}
