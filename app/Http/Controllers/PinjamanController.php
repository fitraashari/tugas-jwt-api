<?php

namespace App\Http\Controllers;

use App\Http\Resources\PinjamanCollection;
use App\Http\Resources\PinjamanResource;
use App\Pinjaman;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pinjaman = Pinjaman::get();
        return new PinjamanCollection($pinjaman);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'buku_id'=>['required'],
            'tanggal_pinjam'=>['required','date'],
            'tanggal_batas_akhir_peminjaman'=>['required','date'],
            // 'tanggal_pengembalian'=>['required'],
            // 'status_ontime'=>['required']
        ]);
        $pinjaman = auth()->user()->pinjaman()->create($this->pinjamanStore());
        return $pinjaman;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjaman $pinjaman)
    {
        //
        return new PinjamanResource($pinjaman);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Pinjaman $pinjaman)
    {
        //
        $pinjaman->update($this->pinjamanStore());
        return new PinjamanResource($pinjaman);
    }

    public function return(Request $request,Pinjaman $pinjaman)
    {
        //
        $pinjaman->update([
            'tanggal_pengembalian' => request('tanggal_pengembalian'),
            'status_ontime' => request('status_ontime')
        ]);
        return new PinjamanResource($pinjaman);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pinjaman $pinjaman)
    {
        //
        $pinjaman->delete();
        return response()->json('Delete Pinjaman Berhasil',200);
    }

    public function pinjamanStore(){
        return [
            'buku_id' => request('buku_id'),
            'tanggal_pinjam' => request('tanggal_pinjam'),
            'tanggal_batas_akhir_peminjaman' => request('tanggal_batas_akhir_peminjaman')
        ];
    }
}
