<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PinjamanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'tanggal_pinjam'=>$this->tanggal_pinjam,
            'tanggal_batas_akhir_peminjaman'=>$this->tanggal_batas_akhir_peminjaman,
            'buku'=>$this->buku,
            'peminjam'=>$this->user,
            'tanggal_pengembalian'=>$this->tanggal_pengembalian,
            'status_ontime'=>$this->status_ontime
        ];
    }
    public function with($request){
        return [
            'message'=>'success'
        ];
    }
}
