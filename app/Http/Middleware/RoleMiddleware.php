<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        //dd($roles);
        $user = Auth::user();
        foreach($roles as $role){
            if($user->checkRole($role)){
                return $next($request);
            }
        }
        return response()->json('Access Denied',403);
        
    }
}
