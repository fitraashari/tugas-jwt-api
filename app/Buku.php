<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    //
    protected $table="buku";
    protected $guarded=[];
    // protected $with = ['pinjaman'];
    public function getRouteKeyName()
    {
        return 'kode_buku';
    }
    public function pinjaman(){
        return $this->hasMany('App\Pinjaman');
    }
    
}
