<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    //
    protected $table="pinjaman";
    protected $guarded=[];
    protected $with = ['buku','user'];
    public function buku(){
        return $this->belongsTo('App\Buku');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }

}
