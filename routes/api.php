<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::namespace('Auth')->group(function(){
    Route::post('register','RegisterController');
    Route::post('login','LoginController');
    Route::post('logout','logoutController');
    // Route::patch('change','ChangeController');
});
Route::get('user','UserController')->middleware('auth:api');

Route::post('create-book','BukuController@store')->middleware('auth:api','role:admin');
Route::patch('update-book/{buku}','BukuController@update')->middleware('auth:api','role:admin');
Route::delete('delete-book/{buku}','BukuController@destroy')->middleware('auth:api','role:admin');

Route::get('book/{buku}','BukuController@show')->middleware('auth:api');
Route::get('book','BukuController@index')->middleware('auth:api');

Route::post('create-rent','PinjamanController@store')->middleware('auth:api');
Route::patch('update-rent/{pinjaman}','PinjamanController@update')->middleware('auth:api');
Route::patch('update-return/{pinjaman}','PinjamanController@return')->middleware('auth:api','role:admin');
Route::delete('delete-rent/{pinjaman}','PinjamanController@destroy')->middleware('auth:api','role:admin');

Route::get('pinjaman/{pinjaman}','pinjamanController@show')->middleware('auth:api');
Route::get('pinjaman','pinjamanController@index')->middleware('auth:api');

Route::post('create-mahasiswa','MahasiswaController@store')->middleware('auth:api','role:admin');
Route::patch('update-mahasiswa/{mahasiswa}','MahasiswaController@update')->middleware('auth:api','role:admin');

Route::post('update-current-mahasiswa','MahasiswaController@updateMe')->middleware('auth:api','role:mahasiswa');

Route::delete('delete-mahasiswa/{mahasiswa}','MahasiswaController@destroy')->middleware('auth:api','role:admin');
Route::get('mahasiswa/{mahasiswa}','MahasiswaController@show')->middleware('auth:api');
Route::get('mahasiswa/','MahasiswaController@index')->middleware('auth:api');